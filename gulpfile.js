var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var phpServer = require('gulp-connect-php');
var browserSync = require('browser-sync').create();

var config = {
    "developmentPath": "Theme/TiekimoUostas/assets/",
    "destinationPath": "Theme/TiekimoUostas/assets/",
    "env": "development",
    "jsDest": 'Theme/TiekimoUostas/assets/js/'
};

gulp.task('styles', function() {
    return gulp.src(config.developmentPath + 'scss')
        .pipe(plumber())
        .pipe(concat('style.scss'))
        .pipe(gulp.dest(config.developmentPath + 'scss'))
});

gulp.task('html', function(){
    gulp.src('Theme/TiekimoUostas/*.html')
        .pipe(plumber())
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('sass', function(){
    gulp.src(config.developmentPath + 'scss/style.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest(config.developmentPath + 'css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('js', function() {
    return gulp.src(config.jsDest + 'script.js')
        .pipe(plumber())
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsDest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('minify-css', function() {
    return gulp.src([
        'Theme/TiekimoUostas/assets/css/bootstrap.min.css',
        'Theme/TiekimoUostas/assets/css/animate.css',
        'Theme/TiekimoUostas/assets/css/font-awesome.min.css',
        'Theme/TiekimoUostas/assets/css/snazzy-info-window.min.css',
        'Theme/TiekimoUostas/assets/css/select2.min.css',
        'Theme/TiekimoUostas/assets/css/jquery.mCustomScrollbar.css',
        'Theme/TiekimoUostas/assets/css/print.css'
        ])
        .pipe(concat('allCss.css'))
        .pipe(gulp.dest(config.developmentPath + 'css'))
        .pipe(rename('allCss.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.developmentPath + 'css'));
});

gulp.task('minify-js', function() {
    return gulp.src([
        'Theme/TiekimoUostas/assets/js/swiper.jquery.min.js',
        // 'page/assets/js/snazzy-info-window.min.js',
        'Theme/TiekimoUostas/assets/js/select2.full.min.js',
        'Theme/TiekimoUostas/assets/js/jquery.print.js',
    ])
        .pipe(concat('allLibs.js'))
        .pipe(gulp.dest(config.jsDest))
        .pipe(rename('allLibs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsDest));
});

gulp.task('default', ['styles', 'sass', 'js', 'html' , 'browserSync'], function (){
    gulp.watch(config.developmentPath + 'scss/*/*.scss', ['sass']);
    gulp.watch(config.developmentPath + 'scss/*.scss', ['sass']);
    gulp.watch('Theme/TiekimoUostas/*.html', ['html']);
    gulp.watch(config.developmentPath + 'js/script.js', ['js']);
});


gulp.task('minify', ['minify-js', 'minify-css']);

gulp.task('serve', function() {
    phpServer.server({ base: 'page', port: 8010, keepalive: true});
});

gulp.task('browserSync', function() {
browserSync.init({
server: {
    baseDir: 'Theme/TiekimoUostas'
},
})
});