<?= ipView('_header.php') ?>
<?php $logistics = ipContent()->getPageByAlias('logistics')  ?>
<?php $tech = ipContent()->getPageByAlias('tech')  ?>
<?php $intro = ipDb()->fetchAll('SELECT * FROM ip_main_page')?>

    <body id="second-page-VH" class="background-img" style="background-image: url('<?= ipThemeUrl('assets/img/intro_bg.jpg') ?>');">

    <section id="intro">
        <div class="container">
            <a href="<?= ipHomeUrl() ?>>" title="iMas"><img class="img-sm" src="<?= ipThemeUrl('assets/img/tiekimo_uostas_logo_l.png') ?>" title="Tiekimo uostas" alt="Tiekimo uostas"  ></a>
            <div class="row mt-40">
                <div class="col-sm-12 col-md-5">
                    <?php
                    $options1   = array(
                        'type'    => 'center',
                        'width'   => 457,
                        'height'  => 640,
                        'quality' => 100,
                    );
                    $thumbnail1 = ipReflection( $intro[0]['image'], $options1 );
                    ?>
                    <div class="background-img logistic-page"
                         style="background-image: url('<?= ipFileUrl($thumbnail1) ?>');">
                        <div class="wrapper">
                            <h1><?= $intro[0]['title'] ?></h1>
                            <a href="<?= ipHomeUrl() . $logistics->getUrlPath()  ?>" class="enter-site" title="Tiekimo Uostas">Į tinklapį</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <a href="<?= ipHomeUrl() ?>" title="Tiekimo uostas" ><img class="img-md" src="<?= ipThemeUrl('assets/img/tiekimo_uostas_logo_l.png') ?>" title="Tiekimo uostas" alt="Tiekimo uostas"  ></a>
                    <?php
                    $options2   = array(
                        'type'    => 'center',
                        'width'   => 652,
                        'height'  => 450,
                        'quality' => 100,
                    );
                    $thumbnail2 = ipReflection( $intro[1]['image'], $options2 );
                    ?>
                    <div class="background-img care-page" style="background-image: url('<?= ipFileUrl($thumbnail2) ?>');">
                        <div class="wrapper">
                            <h1><?= $intro[1]['title'] ?></h1>
                            <a href="<?= ipHomeUrl() . $tech->getUrlPath()  ?>" title="Tiekimo uostas" class="enter-site">Į tinklapį</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <p class="legal-line text-white">© 2017 UAB Tiekimo uostas. Visos teisės saugomos.</p>
                </div>
                <div class="col-sm-12 col-md-6 text-white signature-wrapper">
                    <p class="signature">Tinklapį sukūrė</p>
                    <a href="https://imas.lt" target="_blank" title="iMAs"><img class="img-responsive signature-img" src="<?= ipThemeUrl('assets/img/imas_logo.png') ?>" alt="imas – mobilių aplikacijų ir tinklapių sprendimai" title="imas – mobilių aplikacijų ir tinklapių sprendimai"></a>

<?= ipView('_footer.php')->render() ?>