<?php echo ipView( '_header.php' )->render(); ?>

<?php
$city = ipDb()->fetchAll( "
SELECT * FROM ip_unit_city
" );

?>

<body id="mapPage" data-spy="scroll" data-target=".navbar" data-offset="90">

<section class="background-img" id="home"
         style="background-image: url('<?= ipThemeUrl( '/assets/img/home_bg.jpg' ) ?>')">
    <div class="sizer-wrapper">
        <nav class="navbar navbar-default navbar-custom" data-spy="affix" data-offset-top="20">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" title="Tiekimo uostas" href="<?= ipHomeUrl() ?>">
                        <img class="img-responsive brand-img brand-img-white"
                             src="<?= ipThemeUrl( 'assets/img/tiekimo_uostas_logo_white.png' ) ?>" title="Tiekimo uostas" alt="Tiekimo uostas">
                        <img class="img-responsive brand-img"
                             src="<?= ipThemeUrl( 'assets/img/tiekimo_uostas_logo.png' ) ?>" title="Tiekimo uostas"  alt="Tiekimo uostas">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a title="Pradzia" href="#home">Pradžia</a></li>
						<?php if ( ! ipIsManagementState() ) { ?>
                            <li><a class="" href="#" title="Marsrutai" data-toggle="modal" data-target="#routesModal">Maršrutai ir
                                    grafikai</a></li>
						<?php } ?>
                        <li><a title="Apie mus" href="#about">Apie mus</a></li>
                        <li><a title="Padaliniai" href="#branches">Padaliniai</a></li>
                        <li><a title="Kontaktai" href="#contacts">Kontaktai</a></li>
                        <li><a title="Susisiekti" class="btn btn-rounded" href="#contacts-form">Susisiekti</a></li>
                    </ul>
                </div>

                <div class="nav-backdrop"></div>
            </div>
        </nav>

        <div class="container-fluid home-content">
            <div class="row">
                <div class="content-wrapper col-sm-12 col-md-5">
					<?= ipSlot( 'text', array(
						'id'      => 'home-content-Header',
						'tag'     => 'h1',
						'class'   => 'header-1 text-white',
						'default' => 'Logistikos paslaugos'
					) ) ?>

                    <div class="tb text-white mt-20">
						<?= ipSlot( 'text', array(
							'id'      => 'home-content-paragraph',
							'tag'     => 'p',
							'default' => 'Transporto priemonių atsarginių dalių logistika nuo durų iki durų.'
						) ) ?>
                    </div>

                    <div class="row mt-40">
                        <div class="col-sm-6 feature-entry text-white">
							<?= ipSlot( 'text', array(
								'id'      => 'first-number',
								'tag'     => 'p',
								'class'   => 'fe-big',
								'default' => '4'
							) ) ?>
							<?= ipSlot( 'text', array(
								'id'      => 'first-number-text',
								'tag'     => 'p',
								'default' => 'Padaliniai'
							) ) ?>
                        </div>
                        <div class="col-sm-6 feature-entry text-white">
							<?= ipSlot( 'text', array(
								'id'      => 'second-number',
								'tag'     => 'p',
								'class'   => 'fe-big',
								'default' => '19'
							) ) ?>
							<?= ipSlot( 'text', array(
								'id'      => 'second-number-text',
								'tag'     => 'p',
								'default' => 'Miestų'
							) ) ?>
                        </div>
                        <div class="col-sm-6 feature-entry text-white">
							<?= ipSlot( 'text', array(
								'id'      => 'third-number',
								'tag'     => 'p',
								'class'   => 'fe-big',
								'default' => '2500'
							) ) ?>
							<?= ipSlot( 'text', array(
								'id'      => 'third-number-text',
								'tag'     => 'p',
								'default' => 'Km per parą'
							) ) ?>
                        </div>
                        <div class="col-sm-6 feature-entry text-white">
							<?= ipSlot( 'text', array(
								'id'      => 'fourth-number',
								'tag'     => 'p',
								'class'   => 'fe-big',
								'default' => '6'
							) ) ?>
							<?= ipSlot( 'text', array(
								'id'      => 'fourth-number-text',
								'tag'     => 'p',
								'default' => 'Metų patirtis'
							) ) ?>
                        </div>
                    </div>
					<?php if ( ! ipIsManagementState() ) { ?>
                        <a href="#" id="modalOpener" data-toggle="modal" title="Marsrutai" data-target="#routesModal"
                           class="mt-40 btn btn-main">Žiūrėti
                            grafikus</a>
					<?php } ?>
                </div>
                <div class="map-wrapper col-sm-12 col-md-7">
                    <div class="">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>

        <a href="#about" title="Zemyn" id="scroll-down">Žemyn</a>

    </div>
</section>

<section class="background-img section-padded" id="about"
         style="background-image: url('<?= ipThemeUrl( "assets/img/about_bg.jpg" ) ?>')">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-title',
							'tag'     => 'h2',
							'class'   => 'header-1 text-white',
							'default' => 'Apie mus'
						) ) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="tb tb-header-underlined text-white">
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-Header1',
							'tag'     => 'h2',
							'class'   => 'header-underlined',
							'default' => 'Apie mus'
						) ) ?>
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-text1',
							'tag'     => 'p',
							'default' => 'Lorem ipsum'
						) ) ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="tb tb-header-underlined text-white">
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-Header2',
							'tag'     => 'h2',
							'class'   => 'header-underlined',
							'default' => 'Kokybė'
						) ) ?>
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-text2',
							'tag'     => 'p',
							'default' => 'Mauris neque'
						) ) ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="tb tb-header-underlined text-white">
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-Header3',
							'tag'     => 'h2',
							'class'   => 'header-underlined',
							'default' => 'Patikimumas'
						) ) ?>
						<?= ipSlot( 'text', array(
							'id'      => 'about-content-text3',
							'tag'     => 'p',
							'default' => 'Aenean eget facilisis enim.'
						) ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="background-grey section-padded" id="branches">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
						<?= ipSlot( 'text', array(
							'id'      => 'units-content-title',
							'tag'     => 'h2',
							'class'   => 'header-1',
							'default' => 'Patikimumas'
						) ) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 tabs-pilled-nav">
                    <!-- Nav tabs -->
                    <ul class="list-pills tabs-nav" role="tablist">
						<?php $i = 0;
						foreach ( $city as $cities ) { ?>
							<?php if ( $cities['is_visible'] == 0 ) { ?>
                                <li role="presentation" <?= $i == 0 ? 'class="active"' : '' ?>>
                                    <a href="#<?= $cities['city'] ?>" title="<?= $cities['city'] ?>" aria-controls="<?= $cities['city'] ?>" role="tab"
                                       data-toggle="tab">
										<?= $cities['city'] ?>
                                    </a>
                                </li>
							<?php }
							$i ++;
						} ?>
                        <li role="presentation">
                            <a href="#lietuva" title="Lietuva" aria-controls="lietuva" role="tab" data-toggle="tab">
                                Visa Lietuva
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" id="branches-tab-content">
						<?php $i = 0;
						foreach ( $city as $cities ) {
							?>
							<?php if ( $cities['is_visible'] == 0 ) { ?>
                                <div role="tabpanel" class="tab-pane <?= $i == 0 ? 'active' : '' ?>"
                                     id="<?= $cities['city'] ?>">
									<?php $streets = ipDb()->fetchAll( "
                                      SELECT * FROM ip_unit_street WHERE city_id = {$cities['id']} 
                                    " );
									?>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <h2 class="header-3">
												<?= $cities['city_title'] ?>
                                            </h2>
											<?php foreach ( $streets as $street ) { ?>
                                                <p class="big-text">
													<?= $street['street'] ?>
                                                </p>
											<?php } ?>
                                        </div>
										<?php foreach ( $streets as $street ) {
											$workers = ipDb()->fetchAll( "
                                        SELECT * FROM ip_unit_workers WHERE street_id = {$street['id']}
                                        " )
											?>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="row">
													<?php foreach ( $workers as $worker ) { ?>
                                                        <div class="col-sm-6">
                                                            <ul class="list-person-details">
                                                                <li class="lp-position">
																	<?= $worker['position'] ?>
                                                                </li>
                                                                <li class="lp-name">
																	<?= $worker['fullname'] ?>
                                                                </li>
                                                                <li class="lp-phone">
                                                                    <a href="tel:<?= $worker['phone_number'] ?>"
                                                                       target="_blank" title="Telefonas"
                                                                       class="js-phone-mask"><?= $worker['phone_number'] ?></a>
                                                                </li>
                                                                <li class="lp-email">
                                                                    <a href="mailto:<?= $worker['email'] ?>" title="Pastas"
                                                                       target="_blank"><?= $worker['email'] ?></a>
                                                                </li>
                                                            </ul>
                                                        </div>
													<?php } ?>

                                                </div>
                                            </div>
										<?php } ?>
                                    </div>
                                </div>
							<?php }
							$i ++;
						} ?>
                        <div role="tabpanel" class="tab-pane" id="lietuva">
							<?php $i = 0;
							foreach ( $city as $cities ) {
								?>
								<?php if ( $cities['is_visible'] == 0 ) { ?>
									<?php $streets = ipDb()->fetchAll( "
                                      SELECT * FROM ip_unit_street WHERE city_id = {$cities['id']} 
                                    " );
									?>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <h2 class="header-3">
												<?= $cities['city_title'] ?>
                                            </h2>
											<?php foreach ( $streets as $street ) { ?>
                                                <p class="big-text">
													<?= $street['street'] ?>
                                                </p>
											<?php } ?>
                                        </div>
										<?php foreach ( $streets as $street ) {
											$workers = ipDb()->fetchAll( "
                                        SELECT * FROM ip_unit_workers WHERE street_id = {$street['id']}
                                        " )
											?>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="row">
													<?php foreach ( $workers as $worker ) { ?>
                                                        <div class="col-sm-6">
                                                            <ul class="list-person-details">
                                                                <li class="lp-position">
																	<?= $worker['position'] ?>
                                                                </li>
                                                                <li class="lp-name">
																	<?= $worker['fullname'] ?>
                                                                </li>
                                                                <li class="lp-phone">
                                                                    <a href="tel:<?= $worker['phone_number'] ?>"
                                                                       target="_blank" title="Telefonas"
                                                                       class="js-phone-mask"><?= $worker['phone_number'] ?></a>
                                                                </li>
                                                                <li class="lp-email">
                                                                    <a href="mailto:<?= $worker['email'] ?>" title="Pastas"
                                                                       target="_blank"><?= $worker['email'] ?></a>
                                                                </li>
                                                            </ul>
                                                        </div>
													<?php } ?>
                                                </div>
                                            </div>
										<?php } ?>
                                    </div>
									<?php $i ++;
								}
							} ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="background-blue section-padded" id="contacts">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-sm-12">
							<?= ipSlot( 'text', array(
								'id'      => 'contacts-header',
								'tag'     => 'h2',
								'class'   => 'header-1 text-white',
								'default' => 'Kontaktai'
							) ) ?>
                        </div>

                        <div class="col-sm-12">
                            <div class="tb tb-big-text text-white">
								<?= ipSlot( 'text', array(
									'id'      => 'contacts-administracija',
									'default' => 'Administracija'
								) ) ?>
                                <p class="big">
									<?= ipSlot( 'text', array(
										'id'      => 'contacts-administracija-number1',
										'tag'     => 'a',
										'class'   => 'js-phone-mask phone-link',
										'default' => '+37067466413'
									) ) ?>
                                </p>
                            </div>

                            <div class="tb tb-big-text text-white">
								<?= ipSlot( 'text', array(
									'id'      => 'contacts-emailText',
									'tag'     => 'p',
									'default' => 'El. paštas'
								) ) ?>
                                <p class="big">
									<?= ipSlot( 'text', array(
										'id'      => 'contacts-administracija-email1',
										'tag'     => 'a',
										'class'   => 'js-phone-mask email-link',
										'default' => 'info@tiekimouostas.lt'
									) ) ?>
                                </p>
                            </div>

                            <div class="tb-big-text text-white">
								<?= ipSlot( 'text', array(
									'id'      => 'contacts-requisites',
									'tag'     => 'p',
									'class'   => 'big',
									'default' => 'Įmonės rekvizitai'
								) ) ?>

                                <ul class="list-unstyled">
                                    <li>
										<?= ipSlot( 'text', array(
											'id'      => 'contacts-requisites-name',
											'tag'     => 'p',
											'default' => 'UAB ”Tiekimo Uostas”'
										) ) ?>
                                    </li>
                                    <li>
										<?= ipSlot( 'text', array(
											'id'      => 'contacts-requisites-address',
											'tag'     => 'p',
											'default' => 'Adresas: Pramonės g. 6, LEZ, 94102 Klaipėda'
										) ) ?>
                                    </li>
                                    <li>
										<?= ipSlot( 'text', array(
											'id'      => 'contacts-requisites-code',
											'tag'     => 'p',
											'default' => 'Įmonės kodas: 303480881'
										) ) ?>
                                    </li>
                                    <li>
										<?= ipSlot( 'text', array(
											'id'      => 'contacts-requisites-pvm',
											'tag'     => 'p',
											'default' => 'PVM mokėtojo kodas: LT100009040213'
										) ) ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6" id="contacts-form">
                    <div class="row">
                        <div class="col-sm-12">
							<?= ipSlot( 'text', array(
								'id'      => 'contactHeader',
								'tag'     => 'h2',
								'class'   => 'header1 text-white',
								'default' => 'Susisiekite'
							) ) ?>
                            <span class="success-tu"></span>
                        </div>

                        <div class="col-sm-12">
                            <form class="form-blue" id="tu-connect-form">
                                <div class="form-inline two-inputs">
                                    <div class="form-group custom-placeholder">
                                        <input type="text" name="name" class="form-control">
                                        <span id="tu-name" class="placeholder">Vardas <span
                                                    class="required-text">(būtina)</span></span>

                                    </div>
                                    <div class="form-group custom-placeholder">
                                        <input type="email" name="email" class="form-control">
                                        <span id="tu-email" class="placeholder">El. paštas <span class="required-text">(būtina)</span></span>
                                    </div>
                                </div>
                                <div class="form-inline two-inputs">
                                    <div class="form-group custom-placeholder">
                                        <input type="tel" name="tel" class="form-control">
                                        <span id="tu-tel" class="placeholder">Telefono numeris <span
                                                    class="required-text">(būtina)</span></span>
                                    </div>
                                    <div class="form-group custom-placeholder">
                                        <input id="tu-company" type="text" name="company" class="form-control">
                                        <span class="placeholder">Įmonė</span>
                                    </div>
                                </div>
                                <div class="form-inline">
                                    <div class="form-group custom-placeholder">
                                        <textarea class="form-control text-area" name="text" rows="6"></textarea>
                                        <span id="tu-text" class="placeholder">Žinutė <span
                                                    class="required-text">(būtina)</span></span>
                                    </div>
                                </div>
                                <input type="hidden" name="myEmail"
                                       value=" <?= ipStorage()->get( 'AppControl', 'email' ) ?>">
                                <button id="formSender" type="submit" class="btn btn-main">Siųsti</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="main-footer">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar footer-nav">
                        <ul class="nav navbar-nav">
                            <li><a title="Pradzia" class="active" href="#home">Pradžia</a></li>
							<?php if ( ! ipIsManagementState() ) { ?>
                                <li><a class="" title="Marsrutai" href="#" data-toggle="modal" data-target="#routesModal">Maršrutai ir
                                        grafikai</a></li>
							<?php } ?>
                            <li><a title="Apie mus" href="#about">Apie mus</a></li>
                            <li><a title="Padaliniai" href="#branches">Padaliniai</a></li>
                            <li><a title="Kontaktai" href="#contacts">Kontaktai</a></li>
                            <li><a title="Susisiekti" class="btn btn-rounded" href="#contacts-form">Susisiekti</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a title="Sekite" href="#">
                  <span class="valign">
                    Sekite mus
                  </span>
                                    <i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <p class="legal-line">
						<?= ipSlot( 'text', array(
							'id'      => 'legal',
							'tag'     => 'p',
							'class'   => 'legal-line',
							'default' => '© 2017 UAB Tiekimo uostas. Visos teisės saugomos.'
						) ) ?>
                    </p>
                </div>
                <div class="col-sm-4 signature-wrapper">
                    <p class="signature">
                        Tinklapį sukūrė
                    </p>
                    <a href="https://imas.lt" title="iMas" target="_blank">
                        <img class="img-responsive signature-img" src="<?= ipThemeUrl( 'assets/img/imas_logo.png' ) ?>"
                             alt="iMAS" title="iMas">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php if ( ! ipIsManagementState() ) { ?>
    <!-- Routes Modal -->
    <div class="modal fade modal-custom" id="routesModal" tabindex="-1" role="dialog"
         aria-labelledby="routesModalLabel">
        <div class="modal-dialog modal-lg" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="routesModalLabel">
                        Grafikai
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <h2 class="modal-subheader">Išvyksta iš:</h2>

                            <select id="departureCityValue" class="js-select2-gray">
                                <option value="0" selected>Pasirinkite miestą</option>
								<?php
								$departureCities = ipDb()->fetchAll( "
                              SELECT t1.id, t1.arrival_city_from_title 
                              FROM ip_graphics_grid t1 
                              LEFT JOIN ip_graphics_destination t2 
                              ON t1.id = t2.main_city_id 
                              WHERE t2.arrival_city >= 1 
                              AND t2.main_city_id >= 1 
                              GROUP BY t1.id
                            " );
								?>
								<?php foreach ( $departureCities

								as $departureCity ){ ?>
                                <option value="<?= $departureCity['id'] ?>">
									<?= $departureCity['arrival_city_from_title'] ?><!--</option>-->
									<?php } ?>


                            </select>

                            <ul id="departureTimeFrom" class="list-underlined">

                            </ul>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <h2 class="modal-subheader">Atvyksta į:</h2>

                            <select id="arrivalCityValue" class="js-select2-gray">
                                <option disabled selected>Pasirinkite miestą</option>
                            </select>

                            <ul id="arrivalTimeTo" class="list-underlined">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="print_graphic" href="#" title="Grafikas" class="btn btn-borderless btn-print-icon">
                        Spaudinti grafiką
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php echo ipView( '_footer.php' )->render(); ?>
