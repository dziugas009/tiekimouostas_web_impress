var infoWindow;
if ($('#mapPage').find('.map-wrapper').length > 0) {
    function initializeGoogleMap() {

        var overlay;
        USGSOverlay.prototype = new google.maps.OverlayView();

        function USGSOverlay(bounds, image, map) {

            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;
            this.div_ = null;
            this.setMap(map);
        }

        USGSOverlay.prototype.onAdd = function () {

            var div = document.createElement('div');
            div.style.borderStyle = 'none';
            div.style.borderWidth = '0px';
            div.style.position = 'absolute';

            var img = document.createElement('img');
            img.src = this.image_;
            img.style.width = '100%';
            img.style.height = '100%';
            img.style.position = 'absolute';
            img.style.opacity = '0.6';
            div.appendChild(img);

            this.div_ = div;
            this.div_ = div;
            var panes = this.getPanes();
            panes.overlayLayer.appendChild(div);
        };

        USGSOverlay.prototype.draw = function () {

            var overlayProjection = this.getProjection();
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

            var div = this.div_;
            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = (ne.x - sw.x) + 'px';
            div.style.height = (sw.y - ne.y) + 'px';
        };

        var myLatlng = new google.maps.LatLng(55.329878, 23.905544);
        var myLatlng2 = new google.maps.LatLng(60, 24);

        var LatLng = google.maps.LatLng;
        var mapOptions = {
            // zoom: 7.5,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            backgroundColor: 'transparent',
            disableDefaultUI: true,
            draggable: false,
            scaleControl: false,
            scrollwheel: false,
            styles: [
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [{"visibility": "on"}]
                }, {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [{"visibility": "on"}]
                }, {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "landscape.natural",
                    "elementType": "all",
                    "stylers": [{"visibility": "on"}]
                }, {"featureType": "all", "elementType": "all", "stylers": [{"visibility": "off"}]}
            ]
        };
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var swBound = new google.maps.LatLng(53.891416, 21.016652);
        var neBound = new google.maps.LatLng(56.455583, 26.850769);
        var bounds = new google.maps.LatLngBounds(swBound, neBound);

        map.fitBounds(bounds);
        var srcImage = site + 'Theme/TiekimoUostas/assets/img/map/lithuania.png';
        overlay = new USGSOverlay(bounds, srcImage, map);


        var locationBranchIcon = {
            url: site + 'Theme/TiekimoUostas/assets/img/map/branch-white.png',
            scaledSize: new google.maps.Size(26, 26),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(13, 13),
        };

        var locationBranchActiveIcon = {
            url: site + 'Theme/TiekimoUostas/assets/img/map/branch-orange.png',
            scaledSize: new google.maps.Size(30, 30),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 15),
        };

        var locationSimpleIcon = {
            url: site + 'Theme/TiekimoUostas/assets/img/map/location-white.png',
            scaledSize: new google.maps.Size(10, 10),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(5, 5),
        };

        var locationSimpleActiveIcon = {
            url: site + 'Theme/TiekimoUostas/assets/img/map/location-orange.png',
            scaledSize: new google.maps.Size(18, 18),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(9, 9),
        };

        function returnImg(isBranch, isActive) {
            if (isBranch) {
                if (isActive) {
                    return locationBranchActiveIcon;
                } else {
                    return locationBranchIcon;
                }
            } else {

                if (isActive) {
                    return locationSimpleActiveIcon;
                } else {
                    return locationSimpleIcon;
                }
            }
        }

        function returnOffset(isBranch) {
            var offset;

            if (isBranch) {
                offset = {
                    top: '-40px',
                    left: '0'
                }
            } else {
                offset = {
                    top: '-30px',
                    left: '1px'
                }
            }

            return offset;
        }

        var marker;
        var i;


        //generate locations and times on map
        var map_data = JSON.parse(grid_cities_imas);
        var locations = [];
        var count = 0;
        // console.log(map_data);
        $.each(map_data, function (key, value) {
            if (value['is_main'] == 1) {
                var branch = true;
            } else {
                var branch = false;
            }

            var cityTitleTo = '';
            if (value['departure_city_to_title'] != null) {
                cityTitleTo = value['departure_city_to_title'];
            } else {
                cityTitleTo = value['main_city'];
            }
            var cityTitleFrom = '';
            if (value['arrival_city_from_title'] != null) {
                cityTitleFrom = value['arrival_city_from_title'];
            } else {
                cityTitleFrom = value['main_city'];
            }
            locations[count] = {
                id: value['id'],
                city: cityTitleTo,
                city2: cityTitleFrom,
                lat: value['coord_lat'],
                lng: value['coord_lng'],
                isBranch: branch,
                isModal: value['is_modal'],
                scheduleFrom: value['scheduleTo'],
                scheduleTo: value['scheduleFrom']

            };
            // mapAjax(value['id'], count);
            // mapAjax2(value['id'], count);

            count++;

        });

        var scroller;

        $.each(locations, function (i, e) {
            var position = new google.maps.LatLng(e.lat, e.lng);
            if (locations[i].isBranch == true && locations[i].scheduleTo.length == 0 && locations[i].scheduleFrom.length == 0) {
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: locations[i].city,
                    icon: returnImg(locations[i].isBranch)
                });
                infoWindow = new SnazzyInfoWindow($.extend({}, {
                    marker: marker,
                    // placement: 'right',
                    content: e.city,
                    panOnOpen: true,
                    closeWhenOthersOpen: true,
                    // showCloseButton: false,
                    pointer: false,
                    shadow: false,
                    offset: returnOffset(locations[i].isBranch),
                    maxWidth: 470,
                    edgeOffset: {
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20
                    },
                    callbacks: {
                        afterClose: function () {
                            if ($('.si-content-wrapper').length == 0) {
                                map.fitBounds(bounds);
                            }
                        },
                        beforeClose: function () {
                            scroller = $(this._html.contentWrapper).find('.scroller-wrapper');
                            scroller.mCustomScrollbar("destroy");
                        },
                        afterOpen: function () {
                            scroller = $(this._html.contentWrapper).find('.scroller-wrapper');

                            var selector = scroller.find('.tb-row');

                            scroller.mCustomScrollbar({
                                setHeight: 110,
                                mouseWheel: {
                                    enable: true,
                                    axis: 'y'
                                },
                                liveSelector: '.scroller-wrapper',
                                advanced: {
                                    updateOnContentResize: false,
                                    updateOnSelectorChange: selector
                                },
                                callbacks: {
                                    onInit: function () {
                                        $(this).mCustomScrollbar("update");
                                    }
                                }
                            });
                        }
                    }
                }));
                $.each(e.scheduleFrom, function (j, el) {

                    scheduleFromList += '<li class="tb-row"><span><span class="cell-concat">Iš ' + el.from + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';
                });
                var info =
                    '<div class="row"> ' +
                    '<div class="col-sm-12"> ' +
                    '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                    '<div class="scroller-wrapper"><ul class="list-lined dis-table">Informacija ruošiama</ul></div>' +
                    '</div>' +
                    '</div>'
                ;
                infoWindow.setContent(info);
            } else if (locations[i].isBranch == true && locations[i].isModal == 1) {
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: locations[i].city,
                    icon: returnImg(locations[i].isBranch)
                });
                infoWindow = new SnazzyInfoWindow($.extend({}, {
                    marker: marker,
                    // placement: 'right',
                    content: e.city,
                    panOnOpen: true,
                    closeWhenOthersOpen: true,
                    // showCloseButton: false,
                    pointer: false,
                    shadow: false,
                    offset: returnOffset(locations[i].isBranch),
                    maxWidth: 470,
                    edgeOffset: {
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20
                    },
                    callbacks: {
                        afterClose: function () {
                            if ($('.si-content-wrapper').length == 0) {
                                map.fitBounds(bounds);
                            }
                        },
                        beforeClose: function () {
                            scroller = $(this._html.contentWrapper).find('.scroller-wrapper');
                            scroller.mCustomScrollbar("destroy");
                        },
                        afterOpen: function () {
                            scroller = $(this._html.contentWrapper).find('.scroller-wrapper');

                            var selector = scroller.find('.tb-row');

                            scroller.mCustomScrollbar({
                                setHeight: 110,
                                mouseWheel: {
                                    enable: true,
                                    axis: 'y'
                                },
                                liveSelector: '.scroller-wrapper',
                                advanced: {
                                    updateOnContentResize: false,
                                    updateOnSelectorChange: selector
                                },
                                callbacks: {
                                    onInit: function () {
                                        $(this).mCustomScrollbar("update");
                                    }
                                }
                            });
                        }
                    }
                }));

                var scheduleFromList = '';
                var scheduleToList = '';
                if (locations[i].scheduleTo.length == 0 && locations[i].scheduleFrom.length == 0) {
                    $.each(e.scheduleFrom, function (j, el) {

                        scheduleFromList += '<li class="tb-row"><span><span class="cell-concat">Iš ' + el.from + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';
                    });
                    var info =
                        '<div class="row"> ' +
                        '<div class="col-sm-12"> ' +
                        '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                        '<div class="scroller-wrapper"><ul class="list-lined dis-table">Informacija ruošiama</ul></div>' +
                        '</div>' +
                        '</div>'
                    ;
                    infoWindow.setContent(info);
                } else {


                    $.each(e.scheduleFrom, function (j, el) {

                        scheduleFromList += '<li class="tb-row"><span><span class="cell-concat">Iš ' + el.from + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';
                    });

                    if (locations[i].scheduleTo.length == 0) {
                        var info =
                            '<div class="row"> ' +
                            '<div class="col-sm-12"> ' +
                            '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                            '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleFromList + '</ul></div>' +
                            '</div>' +
                            '</div>'
                        ;
                        infoWindow.setContent(info);
                    } else {
                        $.each(e.scheduleTo, function (j, el) {
                            scheduleToList += '<li class="tb-row"><span><span class="cell-concat">Į ' + el.to + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';

                        });
                        var info =
                            '<div class="row wide-tooltip"> ' +
                            '<div class="col-sm-6"> ' +
                            '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                            '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleFromList + '</ul></div>' +
                            '</div>' +
                            '<div class="col-sm-6"> ' +
                            '<h2 class="popup-header">Iš ' + e.city2 + '</h2>' +
                            '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleToList + '</ul></div>' +
                            '</div>' +
                            '</div>'
                        ;
                        infoWindow.setContent(info);
                    }
                }

            } else if (locations[i].isBranch && locations[i].scheduleTo.length == 0) {
                marker = new google.maps.Marker({
                    id: locations[i].id,
                    position: position,
                    map: map,
                    title: locations[i].city,
                    icon: returnImg(locations[i].isBranch)
                });
            } else {
                if (locations[i].isBranch) {
                    marker = new google.maps.Marker({
                        id: locations[i].id,
                        position: position,
                        map: map,
                        title: locations[i].city,
                        icon: returnImg(locations[i].isBranch)
                    });

                    marker.addListener('click', function () {
                        $('#routesModal').modal();
                        var markerCity = locations[i].id;
                        $('#departureCityValue').val(markerCity).trigger('change');
                    });

                } else {

                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: locations[i].city,
                        icon: returnImg(locations[i].isBranch)
                    });
                    infoWindow = new SnazzyInfoWindow($.extend({}, {
                        marker: marker,
                        // placement: 'right',
                        content: e.city,
                        panOnOpen: true,
                        closeWhenOthersOpen: true,
                        // showCloseButton: false,
                        pointer: false,
                        shadow: false,
                        offset: returnOffset(locations[i].isBranch),
                        maxWidth: 470,
                        edgeOffset: {
                            top: 20,
                            right: 20,
                            bottom: 20,
                            left: 20
                        },
                        callbacks: {
                            afterClose: function () {
                                if ($('.si-content-wrapper').length == 0) {
                                    map.fitBounds(bounds);
                                }
                            },
                            beforeClose: function () {
                                scroller = $(this._html.contentWrapper).find('.scroller-wrapper');
                                scroller.mCustomScrollbar("destroy");
                            },
                            afterOpen: function () {
                                scroller = $(this._html.contentWrapper).find('.scroller-wrapper');

                                var selector = scroller.find('.tb-row');

                                scroller.mCustomScrollbar({
                                    setHeight: 110,
                                    mouseWheel: {
                                        enable: true,
                                        axis: 'y'
                                    },
                                    liveSelector: '.scroller-wrapper',
                                    advanced: {
                                        updateOnContentResize: false,
                                        updateOnSelectorChange: selector
                                    },
                                    callbacks: {
                                        onInit: function () {
                                            $(this).mCustomScrollbar("update");
                                        }
                                    }
                                });
                            }
                        }
                    }));

                    var scheduleFromList = '';
                    var scheduleToList = '';
                    if (locations[i].scheduleTo.length == 0 && locations[i].scheduleFrom.length == 0) {
                        $.each(e.scheduleFrom, function (j, el) {

                            scheduleFromList += '<li class="tb-row"><span><span class="cell-concat">Iš ' + el.from + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';
                        });
                        var info =
                            '<div class="row"> ' +
                            '<div class="col-sm-12"> ' +
                            '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                            '<div class="scroller-wrapper"><ul class="list-lined dis-table">Informacija ruošiama</ul></div>' +
                            '</div>' +
                            '</div>'
                        ;
                        infoWindow.setContent(info);
                    } else {


                        $.each(e.scheduleFrom, function (j, el) {

                            scheduleFromList += '<li class="tb-row"><span><span class="cell-concat">Iš ' + el.from + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';
                        });

                        if (locations[i].scheduleTo.length == 0) {
                            var info =
                                '<div class="row"> ' +
                                '<div class="col-sm-12"> ' +
                                '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                                '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleFromList + '</ul></div>' +
                                '</div>' +
                                '</div>'
                            ;
                            infoWindow.setContent(info);
                        } else {
                            $.each(e.scheduleTo, function (j, el) {
                                scheduleToList += '<li class="tb-row"><span><span class="cell-concat">Į ' + el.to + ' </span></span><span class="line"></span><span><b> ' + el.time + '</b></span>';

                            });
                            var info =
                                '<div class="row wide-tooltip"> ' +
                                '<div class="col-sm-6"> ' +
                                '<h2 class="popup-header">Į ' + e.city + '</h2>' +
                                '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleFromList + '</ul></div>' +
                                '</div>' +
                                '<div class="col-sm-6"> ' +
                                '<h2 class="popup-header">Iš ' + e.city2 + '</h2>' +
                                '<div class="scroller-wrapper"><ul class="list-lined dis-table">' + scheduleToList + '</ul></div>' +
                                '</div>' +
                                '</div>'
                            ;
                            infoWindow.setContent(info);
                        }
                    }
                }
            }
        });
    }
}

$(document).on('ipGoogleMapsLoaded', function () {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = snazzyMapsUrl;
    $(document).find('footer').after(script);
    if ($('#mapPage').find('.map-wrapper').length > 0) {
        initializeGoogleMap();
    }
});

ipLoadGoogleMaps();

$(document).ready(function () {

    var scrollDownIndicator = $('#scroll-down');
    scrollDownIndicator.addClass('visible')
    $(window).scroll(function () {
        if ($(window).scrollTop() > 70 && scrollDownIndicator.hasClass('visible')) {
            scrollDownIndicator.removeClass('visible');
        } else if ($(window).scrollTop() < 70 && !scrollDownIndicator.hasClass('visible')) {
            scrollDownIndicator.addClass('visible');
        }

        $(".scrollto-nav").find('a').each(function () {
            $(this).blur();
        });

    });

    scrollDownIndicator.on('click', function (e) {
        e.preventDefault();
        scrollToPart('#about');
    });


    $(".scrollto-nav").find('a').on('click', function (e) {
        e.preventDefault();
        var section = $(this).attr('href');
        scrollToPart(section);
    });

    function scrollToPart(section) {

        var navHeight;
        if ($(window).width() > 1023) {
            if ($('.ipAdminPanel').length) {
                navHeight = $('.navbar').outerHeight() + $('.ipAdminPanel').outerHeight();
            } else {
                navHeight = $('.navbar').outerHeight();
            }

        } else {
            if ($('.ipAdminPanel').length) {
                navHeight = $('.navbar-header').outerHeight() + $('.ipAdminPanel').outerHeight();
            } else {
                navHeight = $('.navbar-header').outerHeight();
            }
        }

        var scrollPos;
        if (section != '#' && $('body').find(section).length > 0) {
            scrollPos = $(section).offset().top - navHeight;
            $('html, body').stop(true, true).animate({
                scrollTop: scrollPos
            }, 1000);
            $('.navbar-collapse').collapse('hide');
        }
    }


    $('.js-select2-gray').select2({
        width: '100%',
        containerCssClass: 'select-gray',
        dropdownParent: $('.modal')
    });


    $('.custom-placeholder .form-control').keyup(function () {

        if ($(this).val().length) {
            $(this).parents('.form-group').removeClass('has-error');

            $(this).next('.placeholder').hide();
            $(this).next('.error-message').hide();
        } else {
            if ($(this).parents('.form-group').find('.error-message').length < 1 || $(this).parents('.form-group').find('.error-message').text() == '') {
                $(this).next('.placeholder').show();
                $(this).next('.error-message').hide();
            } else {
                $(this).next('.error-message').show();
                $(this).next('.placeholder').hide();
                $(this).parents('.form-group').addClass('has-error');
            }
        }
    });

    $('.placeholder').click(function () {
        $(this).prev('.form-control').focus();
    });

    $('.error-message').click(function () {
        // $(this).hide();
        $(this).prevAll('.form-control').focus();

    });


    $('.tabs-pilled-nav').find('a').each(function () {
        $(this).on('click', function () {
            if (isMobile()) {
                scrollToPart('#branches-tab-content');
            }
        });
    });

    $('.phone-link').attr('href', 'tel:' + $('.phone-link').html());
    $('.phone-link').attr('target', '_blank');


    $('.email-link').attr('href', 'mailto:' + $('.email-link').html());
    $('.email-link').attr('target', '_blank');

    $(document).on("submit", "#tu-connect-form", function (e) {
        sendMsg();
        e.preventDefault();
    });


    function sendMsg() {
        var form = $('#tu-connect-form').serialize();
        $.ajax({
            type: 'POST',
            url: contactUs,
            dataType: 'json',
            data: form,
            success: function (text) {
                $('.error-message').remove();
                $('.form-group').removeClass('has-error')
                $('#tu-' + text.input).after('<span class="error-message">' + text.message + '</span>');
                if (text == '') {
                    console.log('a');

                    $('#tu-connect-form')[0].reset();
                    $('#formSender').html('Pranešimas išsiųstas');
                    $('.placeholder').show();

                } else {
                    $.each(text, function (inx, value) {
                        $('#tu-' + value.input).after('<span class="error-message">' + value.message + '</span>');
                        $('[name="' + value.input + '"]').parents('.form-group').addClass('has-error');
                    });
                }
            }
        });
    }


    // $.ajax({
    //     type: "POST",
    //     url: site + "arrival.php",
    //     dataType: "html",
    //     data: {
    //         'city': $('#departureCityValue').val()
    //     },
    //     success: function (es) {
    //         $('#arrivalCityValue').html(es);
    //         $.ajax({
    //             type: "POST",
    //             url: site + "arrival_step1.php",
    //             dataType: "html",
    //             data: {
    //                 'city': $('#departureCityValue').val(),
    //                 'arrCity': $('#arrivalCityValue').val()
    //             },
    //             success: function (arrival) {
    //                 $('#departureTimeFrom').html(arrival);
    //             }
    //         });
    //         $.ajax({
    //             type: "POST",
    //             url: site + "arrival_step2.php",
    //             dataType: "html",
    //             data: {
    //                 'city': $('#departureCityValue').val(),
    //                 'arrCity': $('#arrivalCityValue').val()
    //             },
    //             success: function (arrival) {
    //                 $('#arrivalTimeTo').html(arrival);
    //             }
    //         });
    //     }
    // });
    // console.log($('#departureCityValue').val());
    // $.ajax({
    //     type: 'POST',
    //     url: 'http://tiekimouostas.imas.lt/dep_city.php',
    //     dataType: 'html',
    //     data: {
    //         'city': $('#departureCityValue').val()
    //     },
    //     success: function (text) {
    //         $('#departureTimeFrom').html(text)
    //     }
    // });

    $('#departureCityValue').on('change', function () {
        $('#departureTimeFrom').html('');
        $('#arrivalTimeTo').html('');
        $.ajax({
            type: 'POST',
            url: site + 'dep_city.php',
            dataType: 'html',
            data: {
                'city': $(this).val()
            },
            success: function (text) {

                $.ajax({
                    type: "POST",
                    url: site + "arrival.php",
                    dataType: "html",
                    data: {
                        'city': $('#departureCityValue').val()
                    },
                    success: function (es) {
                        $('#arrivalCityValue').html(es)
                        $.ajax({
                            type: "POST",
                            url: site + "arrival_step1.php",
                            dataType: "html",
                            data: {
                                'city': $('#departureCityValue').val(),
                                'arrCity': $('#arrivalCityValue').val()
                            },
                            success: function (arrival) {
                                $('#departureTimeFrom').html(arrival);
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: site + "arrival_step2.php",
                            dataType: "html",
                            data: {
                                'city': $('#departureCityValue').val(),
                                'arrCity': $('#arrivalCityValue').val()
                            },
                            success: function (arrival) {
                                $('#arrivalTimeTo').html(arrival);
                            }
                        });
                    }
                });

            }
        });

    });
    $('#arrivalCityValue').on('change', function () {
        $.ajax({
            type: "POST",
            url: site + "arrival_step1.php",
            dataType: "html",
            data: {
                'city': $('#departureCityValue').val(),
                'arrCity': $('#arrivalCityValue').val()
            },
            success: function (arrival) {
                $('#departureTimeFrom').html(arrival);
            }
        });
        $.ajax({
            type: "POST",
            url: site + "arrival_step2.php",
            dataType: "html",
            data: {
                'city': $('#departureCityValue').val(),
                'arrCity': $('#arrivalCityValue').val()
            },
            success: function (arrival) {
                $('#arrivalTimeTo').html(arrival);
            }
        });
    });


    $('#print_graphic').on('click', function () {
        $('#routesModal').printThis({
            debug: false,
            importCSS: true,
            importStyle: true,
            printContainer: true,
            loadCSS: site + 'Theme/TiekimoUostas/assets/css/style.css',
            pageTitle: "Grafikai",
            removeInline: false,
            printDelay: 100,
            header: null,
            formValues: true
        });
    });

    $('body').on('shown.bs.modal', '.modal', function () {
        $(this).find('select').focus();
    });

    $('#modalOpener').on('click', function () {
        $('#departureCityValue').val(0).trigger('change');
    });

});

function loadTimes() {
    $.ajax({
        type: 'POST',
        url: site + "arrival.php",
        dataType: 'html',
        data: {
            'arrCityVal': $('#arrivalCityValue').val()
        },
        success: function (text) {
            $('#arrivalTimeTo').html(text)
        }
    });
}


function isMobile() {
    if ($(window).width() < 992) {
        return true;
    }
    else {
        return false;
    }
}