<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FAVICON -->

    <!-- Stylesheets -->
	<?php ipAddCss( 'assets/css/allCss.min.css?v=0906' ) ?>
    <!--	<link href="assets/css/allCss.min.css?v=0905" rel="stylesheet">-->

    <script>
        var site = <?= "'".ipHomeUrl()."'" ?>;
        var contactUs = <?= "'".ipHomeUrl()."susisiekite.php'" ?>;

    </script>
    <!-- Custom Styles -->
    <!--<link href="assets/css/style.min.css?v=0905" rel="stylesheet">-->
	<?php ipAddCss( 'assets/css/style.min.css' ) ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108797659-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108797659-1');
</script>

	<?php echo ipHead()?>
</head>
