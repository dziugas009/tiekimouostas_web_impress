<?= ipView('_header.php')->render() ?>

<?php $aboutUs = ipDb()->fetchAll('SELECT * FROM ip_about_us') ?>
<body id="secondPageTiekimoUostas" data-spy="scroll" data-target=".navbar" data-offset="90">

<section class="background-img" id="pradzia"
         style="background-image: url('<?= ipThemeUrl('assets/img/home_care_bg.jpg') ?>'); background-position: left;">
    <div class="sizer-wrapper">
        <nav class="navbar navbar-default navbar-custom" data-spy="affix" data-offset-top="20">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a title="Tiekimo uostas"  class="navbar-brand" href="<?= ipHomeUrl() ?>">
                        <img class="img-responsive brand-img brand-img-white"
                             src="<?= ipThemeUrl('assets/img/tiekimo_uostas_logo_white.png') ?>" title="Tiekimo uostas"  alt="Tiekimo uostas">
                        <img class="img-responsive brand-img"
                             src="<?= ipThemeUrl('assets/img/tiekimo_uostas_logo.png') ?>" title="Tiekimo uostas"  alt="Tiekimo uostas">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a title="Pradžia"  href="#pradzia">Pradžia</a></li>
                        <li><a title="Atliekama apžiūra"  href="#prieziura">Atliekama apžiūra</a></li>
                        <li><a title="Teikiamos paslaugos"  href="#paslaugos">Teikiamos paslaugos</a></li>
                        <li><a title="Sertifikatai"  href="#sertifikatai">Sertifikatai</a></li>
                        <li><a title="Kontaktai"  class="btn btn-rounded btn-care" href="#kontaktai">Kontaktai</a></li>
                    </ul>
                </div>

                <div class="nav-backdrop"></div>
            </div>
        </nav>

        <div class="container-fluid home-content">
            <div class="row">
                <div class="content-wrapper col-sm-12 col-md-7">
                    <h1 class="header-1 main text-white text-uppercase">
                        <?= ipSlot('text', array(
                            'id' => 'second_page_intro',
                            'tag' => 'span',
                            'class' => '',
                            'default' => 'Profesionali priežiūra'
                        )) ?>
                        <?= ipSlot('text', array(
                            'id' => 'second_page_intro2',
                            'tag' => 'span',
                            'class' => '',
                            'default' => 'Jūsų technikai'
                        )) ?>
                        <!--                        <span>Profesionali</span> priežiūra<br><span>Jūsų</span> technikai-->
                    </h1>

                    <div class="tb text-white mt-40 certificate-wrapper">

                        <?php $options = [
                            'id'        => 'second_page_certificate_logo',
                            'class'     => 'img-responsive certificate-img',
                            'alt'       => 'Esame sertifikuota technikos priežiūros tarnyba',
                            'default'   => ipThemeUrl('assets/img/certificate-icon.png')
                        ]; ?>
                        <?= ipSlot('image', $options) ?>
                        <?= ipSlot('text', array(
                            'id'        => 'second_page_text_logo',
                            'tag'       => 'p',
                            'class'     => 'certificate',
                            'default'   => 'Esame sertifikuota technikos priežiūros tarnyba'
                        )); ?>
                    </div>

                </div>
            </div>
        </div>

        <a title="Zemyn" href="#prieziura" id="scroll-down">Žemyn</a>

    </div>
</section>

<section class="section-padded" id="prieziura">
    <div class="sizer-wrapper-sm-full">
        <div class="container-fluid">

            <div id="prieziura">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-md-push-6">
                        <div class="list-table">
                            <h2 class="header-2 text-underline text-white text-uppercase">
                                <?= ipSlot('text', array(
                                    'id'        => 'second_page_review_aboutUs',
                                    'tag'       => 'span',
                                    'class'     => '',
                                    'default'   =>'Atliekama priežiūra'
                                )) ?>
                            </h2>
                            <div class="list-check text-white unstyled-list-wrapper">
                                <?= $aboutUs[0]['text'] ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-md-pull-6">
                        <?php $options = [
                            'type' => 'center',
                            'width' => 695,
                            'height' => 490,
                            'quality' => 100,
                        ];
                        $thumbnail = ipReflection($aboutUs[0]['image'], $options)
                        ?>
                        <div class="background-img"
                             style="background-image: url('<?= ipFileUrl($thumbnail) ?>'); min-height: 490px;"></div>
                    </div>
                </div>
            </div>

            <div id="paslaugos">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="list-table">
                            <h2 class="header-2 text-underline text-white text-uppercase">
                                <?= ipSlot('text', array(
                                    'id'        => 'second_page_offer_aboutUs',
                                    'tag'       => 'span',
                                    'class'     => '',
                                    'default'   =>'Siūlomos paslaugos'
                                )) ?>
                            </h2>
                            <div class="list-check text-white unstyled-list-wrapper">
                                <?= $aboutUs[1]['text'] ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <?php $options = [
                            'type' => 'center',
                            'width' => 695,
                            'height' => 490,
                            'quality' => 100,
                        ];
                        $thumbnail = ipReflection($aboutUs[1]['image'], $options)
                        ?>
                        <div class="background-img"
                             style="background-image: url('<?= ipFileUrl($thumbnail) ?>'); min-height: 490px;"></div>
                    </div>
                </div>
            </div>
            <?php if ($aboutUs[2]) { ?>
                <div id="sertifikatai">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-md-push-6">
                            <div class="list-table">
                                <h2 class="header-2 text-underline text-white text-uppercase">
                                    <?= ipSlot('text', array(
                                        'id'        => 'second_page_certificates_aboutUs',
                                        'tag'       => 'span',
                                        'class'     => '',
                                        'default'   =>'Mūsų sertifikatai'
                                    )) ?>
                                </h2>
                                <ul class="list-document text-white">
                                    <?php
                                    $exs = json_decode($aboutUs[2]['docs']);
                                    if (!empty($exs)){
                                        foreach ($exs as $ex){
                                            echo '<li><a href="'. ipFileUrl('file/repository/'. $ex) . '" download>'. $ex . "</a></li>";
                                        }
                                    } else {
                                        echo '<p>Šiuo metu nieko nėra įkelta.</p>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-md-pull-6">
                            <div class="background-img"
                                 style="background-image: url('<?= ipThemeUrl('assets/img/award.jpg') ?>'); min-height: 490px;"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>


<section class="background-img section-padded" id="kontaktai"
         style="background-image: url('<?= ipThemeUrl('assets/img/contacts_ltu_bg.jpg') ?>'); background-position: left;">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="header-1 text-white text-uppercase">
                        <?= ipSlot('text', array(
                            'id' => 'second_page_contact_us',
                            'tag' => 'span',
                            'class' => '',
                            'default' => 'Susisiekite su mumis'
                        )) ?>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="tb tb-big-text text-white">
                        <p class="big tel-red">
                            <a title="Phone" href="tel:<?= ipStorage()->get('AppControl', 'phone') ?>" class="js-phone-mask">
                                <?= ipStorage()->get('AppControl', 'phone') ?>
                            </a>
                        </p>
                        <?= ipSlot('text', array(
                            'id' => 'second_page_textAfterNo',
                            'tag' => 'p',
                            'class' => '',
                            'default' => 'Laukiame jūsų skambučio aukščiau pateiktu telefono numeriu.'
                        )) ?>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12 col-md-6 mt-40">
                    <div class="tb-big-text">
                        <?= ipSlot('text', array(
                            'id' => 'contacts-requisites',
                            'tag' => 'p',
                            'class' => 'big caption-red',
                            'default' => 'Įmonės rekvizitai'
                        )) ?>

                        <ul class="list-unstyled list-care-contacts">
                            <li>
                                <?= ipSlot('text', array(
                                    'id' => 'contacts-requisites-name',
                                    'tag' => 'p',
                                    'default' => 'UAB ”Tiekimo Uostas”'
                                )) ?>
                            </li>
                            <li>
                                <?= ipSlot('text', array(
                                    'id' => 'contacts-requisites-address',
                                    'tag' => 'p',
                                    'default' => 'Adresas: Pramonės g. 6, LEZ, 94102 Klaipėda'
                                )) ?>
                            </li>
                            <li>
                                <?= ipSlot('text', array(
                                    'id' => 'contacts-requisites-code',
                                    'tag' => 'p',
                                    'default' => 'Įmonės kodas: 303480881'
                                )) ?>
                            </li>
                            <li>
                                <?= ipSlot('text', array(
                                    'id' => 'contacts-requisites-pvm',
                                    'tag' => 'p',
                                    'default' => 'PVM mokėtojo kodas: LT100009040213'
                                )) ?>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 mt-40">
                    <div class="tb-big-text">
                        <?= ipSlot('text', array(
                            'id' => 'second_page_info1',
                            'tag' => 'p',
                            'class' => 'big caption-red',
                            'default' => 'Kontaktinė informacija'
                        )) ?>
                        <ul class="list-unstyled list-care-contacts">
                            <li>
                                <?= ipSlot('text', array(
                                    'id' => 'second_page_info2',
                                    'tag' => 'p',
                                    'class' => '',
                                    'default' => 'Phasellus vulputate sapien elementum, malesuada risus et, gravida nisl. Nulla commodo in ipsum eu rutrum.'
                                )) ?><br><br>
                            </li>
                            <li>
                                El. paštas: <a
                                        title="Pastas" href="mailto:<?= ipStorage()->get('AppControl', 'email') ?>"><?= ipStorage()->get('AppControl', 'email') ?></a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
    </div>
    </div>
</section>

<footer class="main-footer care">
    <div class="sizer-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar footer-nav">
                        <ul class="nav navbar-nav">
                            <li><a title="Pradžia" href="#pradzia">Pradžia</a></li>
                            <li><a title="Prieziura" href="#prieziura">Atliekama apžiūra</a></li>
                            <li><a title="Paslaugos" href="#paslaugos">Teikiamos paslaugos</a></li>
                            <li><a title="Sertifikatai" href="#sertifikatai">Sertifikatai</a></li>
                            <li><a title="Kontaktai" href="#kontaktai">Kontaktai</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a title="Follow" href="#">
                  <span class="valign">
                    Sekite mus
                  </span>
                                    <i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <p class="legal-line">© 2017 UAB Tiekimo uostas. Visos teisės saugomos.
                    </p>
                </div>
                <div class="col-sm-4 signature-wrapper">
                    <p class="signature">Tinklapį sukūrė</p>
                    <a href="https://imas.lt" target="_blank" title="iMas" ><img class="img-responsive signature-img"
                                                                                 src="<?= ipThemeUrl('assets/img/imas_logo.png') ?>"
                                                                                 alt="imas – mobiliųjų aplikacijų ir tinklapių sprendimai" title="imas – mobiliųjų aplikacijų ir tinklapių sprendimai"></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?= ipView('_footer.php')->render() ?>
