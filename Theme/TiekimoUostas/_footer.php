
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>


<!-- All libs -->

<?php if ( ! ( ipIsManagementState() ) ) { ?>
	<?php
	ipAddJs( 'assets/js/jquery-3.2.1.min.js' );
	ipAddJs( 'assets/js/bootstrap.min.js' );
} ?>
<?php
$pdo = ipDb()->getConnection();
$raw = $pdo->prepare( "
      SELECT
        t1.id,
        t1.main_city,
        t1.arrival_city_from_title,
        t1.departure_city_to_title,
        t1.is_main,
        t1.is_modal,
        t1.coord_lat,
        t1.coord_lng
      FROM
        ip_graphics_grid t1
    " );
$raw->execute();
$cities = $raw->fetchAll( \PDO::FETCH_FUNC, function () {
	$args = func_get_args();


	$deprartures = ipDb()->fetchAll( "SELECT 
  dest.id, 
  DATE_FORMAT(dest.departure_time, '%H:%i') as departure_time,
  DATE_FORMAT(dest.arrival_time, '%H:%i') as arrival_time,
  city.main_city,
  city.departure_city_to_title
  FROM ip_graphics_destination dest, ip_graphics_grid city
  WHERE dest.arrival_city = city.id AND dest.main_city_id = {$args[0]}
  GROUP BY dest.departure_time ASC" );
	$mapFrom     = [];
	$i           = 1;


	foreach ( $deprartures as $deprarture ) {
		if ( $deprarture['departure_city_to_title'] != null && ! empty( $deprarture['departure_city_to_title'] ) ) {
			$cityTitle = $deprarture['departure_city_to_title'];
		} else {
			$cityTitle = $deprarture['main_city'];
		}
		array_push( $mapFrom, $mapFrom[ $i ] = array(
			'to'   => (string) $cityTitle,
			'time' => $deprarture['departure_time'],
		) );
		$i ++;
	}
	array_pop( $mapFrom );

	$arrivals = ipDb()->fetchAll( "SELECT 
  dest.id,
  DATE_FORMAT(dest.departure_time, '%H:%i') as departure_time,
  DATE_FORMAT(dest.arrival_time, '%H:%i') as arrival_time,
  city.main_city,
  city.arrival_city_from_title
  FROM ip_graphics_destination dest, ip_graphics_grid city 
  WHERE dest.main_city_id = city.id AND dest.arrival_city = {$args[0]}
  GROUP BY dest.departure_time ASC" );
	$mapTo    = [];
	$j        = 1;


	foreach ( $arrivals as $arrival ) {
		if ( $arrival['arrival_city_from_title'] != null && ! empty( $arrival['arrival_city_from_title'] ) ) {
			$cityTitle = $arrival['arrival_city_from_title'];
		} else {
			$cityTitle = $arrival['main_city'];
		}
		array_push( $mapTo, $mapTo[ $j ] = array(
			'from' => (string) $cityTitle,
			'time' => $arrival['arrival_time'],
		) );
		$j ++;
	}
	array_pop( $mapTo );

	return [
		'id'                      => $args[0],
		'main_city'               => $args[1],
		'arrival_city_from_title' => $args[2],
		'departure_city_to_title' => $args[3],
		'is_main'                 => $args[4],
		'is_modal'                => $args[5],
		'coord_lat'               => $args[6],
		'coord_lng'               => $args[7],
		'scheduleFrom'            => $mapFrom,
		'scheduleTo'              => $mapTo
	];
} );
?>
<?php
//print_r( $cities );
ipAddJsVariable( 'grid_cities_imas', json_encode( $cities ) ) ?>
<!-- Main -->
<!--<script src="assets/js/script.min.js?v=0905"></script>-->
<?php ?>
<?php
ipAddJsVariable( 'snazzyMapsUrl', ipThemeUrl( 'assets/js/snazzy-info-window.min.js?v=0907' ) );
ipAddJs( 'assets/js/allLibs.min.js' );
ipAddJs( 'assets/js/select2.full.min.js?v=0907' );
//ipAddJs( 'assets/js/jquery.print.js?v=0907' );
ipAddJs( 'assets/js/jquery.mCustomScrollbar.min.js' );
ipAddJs( 'assets/js/jquery.mousewheel.min.js' );
ipAddJs( 'assets/js/script.min.js' );

?>
<?php echo ipJs(); ?>
</body>
</html>
