<?php
require_once __DIR__ . '/config2.php';
include( __DIR__ . '/config2.php' );
$map = [];
//AND  departure_time >= TIME(NOW())  Jeigu nori atvaizduoti esamus
$departureCities = "SELECT dest.id, DATE_FORMAT(dest.departure_time, '%H:%i') as departure_time, DATE_FORMAT(dest.arrival_time, '%H:%i') as arrival_time,
 city.main_city, city.departure_city_to_title FROM ip_graphics_destination dest, ip_graphics_grid city
WHERE dest.arrival_city = city.id AND dest.main_city_id = {$_POST['mapCityId']}
GROUP BY dest.arrival_time ASC
";
$i               = 1;
$result          = $conn->query( $departureCities );
$cityTitle       = '';

while ( $row = $result->fetch_assoc() ) {
	if ( $row['departure_city_to_title'] != null && ! empty( $row['departure_city_to_title'] ) ) {
		$cityTitle = $row['departure_city_to_title'];
	} else {
		$cityTitle = $row['main_city'];
	}
	array_push( $map, $map[ $i ] = array(
		'departureTime' => $row['departure_time'],
		'arrivalTime'   => $row['arrival_time'],
		'city'          => $cityTitle,
	) );
	$i ++;

};
array_pop( $map );
print_r( json_encode( $map ) );
$conn->close();