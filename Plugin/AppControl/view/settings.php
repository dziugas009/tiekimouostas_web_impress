
<div class="col-md-4 col-md-pull-0">
    <h1>Kontaktiniai duomenys</h1>
    <br/>
    <?php if (isset($success) && $success) { ?>
        <div class="alert alert-success">Sėkmingai išsaugota.</div>
    <?php } ?>

    <div class="well">
        <?= $form->render() ?>
    </div>
</div>
