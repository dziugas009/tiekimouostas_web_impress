<?php
/**
 * Adds administration grid
 *
 * When this plugin is installed, `Partners` panel appears in administration site.
 *
 */

namespace Plugin\AppControl;


use Ip\Form\Field\Email;
use Ip\Form\Field\Hidden;
use Ip\Form\Field\RepositoryFile;
use Ip\Form\Field\RichText;
use Ip\Form\Field\Submit;
use Ip\Form\Field\Text;
use Ip\Form\Fieldset;
use Ip\Response\Json;

class AdminController {

	/**
	 * @ipSubmenu Kontaktiniai duomenys
	 */

	public function index() {
		$layout = ipView( 'view/settings.php' );

		$form = $this->settingsForm();

		if ( isset( $_SESSION['AppControl.saved'] ) && $_SESSION['AppControl.saved'] ) {
			$layout->setVariable( 'success', true );
		}

		$_SESSION['AppControl.saved'] = false;

		$layout->setVariable( 'form', $form );

		$layout->render();

		return $layout;
	}

	private function settingsForm() {
		$form = new \Ip\Form();

		$form->addField( new Hidden( [
			'name'  => 'aa',
			'value' => 'AppControl.submitSettings'
		] ) );

		$langs = ipContent()->getLanguages();

		foreach ( $langs as $lang ) {
			$fieldset = new Fieldset();

			$fieldset->addField( new Email( [
				'label' => 'El.paštas',
				'name'  => 'email',
				'value' => ipStorage()->get( 'AppControl', 'email', 'info@tiekimouostas.lt' )
			] ) );
            $fieldset->addField( new Text( [
                'label' => 'Numeris',
                'name'  => 'phone',
                'value' => ipStorage()->get( 'AppControl', 'phone', '+37067466413' )
            ] ) );

			$form->addFieldset( $fieldset );


		}

		$form->addField( new Submit( [
			'value'      => 'Išsaugoti',
			'attributes' => [
				'style' => 'margin-top:30px;',
				'class' => 'btn btn-success'
			]
		] ) );

		return $form;
	}

	public function submitSettings() {
		ipRequest()->mustBePost();
		$post = ipRequest()->getPost();

		$form   = $this->settingsForm();
		$errors = $form->validate( $post );

		if ( ! empty( $errors ) ) {
			$data = array(
				'status' => 'error',
				'errors' => $errors
			);

			return new Json( $data );
		}

		$_SESSION['AppControl.saved'] = true;

		$skip_fields = [
			'securityToken',
			'antispam',
			'aa',
		];

		foreach ( $post as $field => $value ) {
			if ( ! in_array( $field, $skip_fields ) ) {
				ipStorage()->set( 'AppControl', $field, $value );
			}
		}

		$data = array(
			'status'      => 'ok',
			'redirectUrl' => '/?aa=AppControl.index'
		);

		return new Json( $data );
	}

}