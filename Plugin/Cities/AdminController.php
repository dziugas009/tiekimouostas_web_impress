<?php

namespace Plugin\Cities;

class AdminController {
	public function index() {
		$config = array(
			'title' => 'Miestai',
			'table' => 'graph_cities',
			'fields' => array(
				array(
					'label' => 'Miesto pavadinimas',
					'field' => 'city_title'
				),
				array(
					'label' => 'Ar tai pagrindinis miestas?',
					'field' => 'is_main',
					'type' => 'Checkbox'
				),
				array(
					'label' => 'Koordinatės (platuma)',
					'field' => 'coord_lat',
					'preview' => false
				),
				array(
					'label' => 'Koordinatės (ilgumos)',
					'field' => 'coord_lng',
					'preview' => false
				)
			)
		);
		return ipGridController($config);
	}
}
