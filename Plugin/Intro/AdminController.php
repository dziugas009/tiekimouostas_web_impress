<?php
/**
 * Created by PhpStorm.
 * User: mb1
 * Date: 12/10/2017
 * Time: 09:46
 */

namespace Plugin\Intro;

class AdminController
{
    public function index()
    {
        $config = [
            'title' => 'Pagrindinis puslapis',
            'table' => 'main_page',
            'allowCreate' => false,
            'allowSearch' => false,
            'allowDelete' => false,
            'fields' => [
                [
                    'label' => 'Pavadinimas',
                    'field' => 'title'
                ],
                [
                    'label' => 'Paveiksliukas',
                    'field' => 'image',
                    'type'  => 'RepositoryFile'
                ]
            ]
        ];
        return ipGridController($config);
    }
}