<?php

namespace Plugin\Units;


class AdminController {
	public function index() {
		$config = array(
			'title'  => 'Padalinių informacija',
			'table'  => 'unit_city',
			'fields' => array(
				array(
					'label' => 'Mietas',
					'field' => 'city',
				),
				array(
					'label' => 'Pavadinimas',
					'field' => 'city_title'
				),
				array(
					'label' => 'Ar slėpti?',
					'field' => 'is_visible',
					'type' => 'Checkbox'
				),
				array(
					'label'  => 'Padaliniai',
					'type'   => 'Grid',
					'field'  => 'city_id',
					'config' => array(
						'title'           => 'Padaliniai',
						'connectionField' => 'city_id',
						'table'           => 'unit_street',
						'fields'          => array(
							array(
								'label' => 'Gatvė',
								'field' => 'street'
							),
							array(
								'label'  => 'Darbuotojai',
								'type'   => 'Grid',
								'field'  => 'street_id',
								'config' => array(
									'title'           => 'Darbuotojai',
									'connectionField' => 'street_id',
									'table'           => 'unit_workers',
									'fields'          => array(
										array(
											'label' => 'Pareigos',
											'field' => 'position'
										),
										array(
											'label' => 'Vardas',
											'field' => 'fullname'
										),
										array(
											'label' => 'Telefono numers',
											'field' => 'phone_number'
										),
										array(
											'label' => 'El. paštas',
											'field' => 'email'
										)
									)
								)
							)
						)
					)
				)

			)
		);

		return ipGridController( $config );
	}

}