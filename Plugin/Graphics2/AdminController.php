<?php
namespace Plugin\Graphics2;
use Plugin\DateField\GridDateField;
define('CLASS', 'AdminController');
class AdminController {

	public function index() {
		$cities    = ipDb()->selectAll( 'graphics_grid', array(
			'id',
			'main_city'
		), array());
		$city_list = array( array( '0', '--' ) );
		foreach ( $cities as $city ) {
			array_push( $city_list, array( $city['id'], $city['main_city'] ) );
		}
		$config = array(
			'title'  => 'Maršrutų informacija',
			'table'  => 'graphics_grid',
			'createPosition' => 'bottom',
			'fields' => array(
				array(
					'label' => 'Miestas',
					'field' => 'main_city',
					'validators' => array('Required')
				),
				array(
					'label' => 'Atvyksta iš (pvz. Klaipėdos)',
					'field' => 'arrival_city_from_title',
					'preview' => false
				),
				array(
					'label' => 'Išvyksta į (pvz. Klaipėdą)',
					'field' => 'departure_city_to_title',
					'preview' => false
				),
				array(
					'label' => 'Ar tai pagrindinis miestas?',
					'field' => 'is_main',
					'type'  => 'Checkbox'
				),
				array(
					'label' => 'Ar informaciją rodyti grafike? (JEIGU TAI PAGRINDINIS MIESTAS)',
					'field' => 'is_modal',
					'type' => 'Checkbox',
					'preview' => false
				),
				array(
					'label'   => 'Koordinatės (platuma)',
					'field'   => 'coord_lat',
					'preview' => false,
					'validators' => array('Required')
				),
				array(
					'label'   => 'Koordinatės (ilgumos)',
					'field'   => 'coord_lng',
					'preview' => false,
					'validators' => array('Required')
				),
				array(
					'label'  => 'Maršrutai',
					'type'   => 'Grid',
					'field'  => 'main_city_id',
					'config' => array(
						'title'           => 'Maršrutai',
						'connectionField' => 'main_city_id',
						'table'           => 'graphics_destination',
						'createPosition' => 'bottom',
						'fields'          => array(
							array(
								'type'  => 'Plugin\DateField\GridTimeField',
								'label' => 'Išvykimo laikas',
								'field' => 'departure_time',
								'preview' => function ($value){
									if ($value == null || empty($value) || $value == '00:00:00'){
										return '';
									}  else {
										$parts = explode(":", $value);
										return $parts[0].':'.$parts[1];
									}
								}
							),
							array(
								'label'  => 'Atvykimo miestas',
								'field'  => 'arrival_city',
								'type'   => 'Select',
								'values' => $city_list
							),
							array(
								'type'  => 'Plugin\DateField\GridTimeField',
								'label' => 'Atvykimo laikas',
								'field' => 'arrival_time',
								'preview' => function ($value){
									if ($value == null || empty($value) || $value == '00:00:00'){
										return '';
									}  else {
										$parts = explode(":", $value);
										return $parts[0].':'.$parts[1];
									}
								}
							)
						)
					)
				)
			)
		);

		return ipGridController( $config );

	}
	public static function previewTime($value){
		if ($value == null || empty($value) || $value == '00:00:00'){
			return '';
		}  else {
			$parts = explode(":", $value);
			return $parts[0].':'.$parts[1];
		}
	}
}