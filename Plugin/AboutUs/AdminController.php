<?php
/**
 * Created by PhpStorm.
 * User: mb1
 * Date: 12/10/2017
 * Time: 11:04
 */

namespace Plugin\AboutUs;


class AdminController
{
    public function index()
    {
        $config = [
            'title' => 'Apie mus',
            'table' => 'about_us',
            'fields' => [
                [
                    'label' => 'Pavadinimas',
                    'field' => 'title'
                ],
                [
                    'label'     => 'Tekstas',
                    'field'     => 'text',
                    'type'      => 'RichText',
                    'preview'   => false
                ],
                [
                    'label' => 'Paveiksliukas',
                    'field' => 'image',
                    'type'  => 'RepositoryFile'
                ],
                [
                    'label'     => 'Sertifikatai',
                    'field'     => 'docs',
                    'type'      => 'RepositoryFile',
                    'fileLimit' => -1,
                    'preview'   => false
                ]
            ]
        ];
        return ipGridController($config);
    }
}