/**
 * On/Off field require JS to function. But some forms might appear on the page using AJAX so we can't rely on document ready event.
 * ImpressPages has special 'ipInitForms' event to notify all JS libraries that there are new forms appeared on the page.
 * Here we catch 'ipInitForms' event and initialize On/Off related js
 */
$(document).on('ipInitForms', function() {
    "use strict";	
	if ($.datepicker){
		$.datepicker.regional['lt'] = {
			closeText: 'Uždaryti',
			prevText: '&#x3c;Atgal',
			nextText: 'Pirmyn&#x3e;',
			currentText: 'Šiandien',
			monthNames: ['Sausio','Vasario','Kovo','Balandžio','Gegužės','Birželio',
			'Liepos','Rugpjūčio','Rugsėjo','Spalio','Lapkričio','Gruodžio'],
			monthNamesShort: ['Sau','Vas','Kov','Bal','Geg','Bir',
			'Lie','Rugp','Rugs','Spa','Lap','Gru'],
			dayNames: ['sekmadienis','pirmadienis','antradienis','trečiadienis','ketvirtadienis','penktadienis','šeštadienis'],
			dayNamesShort: ['sek','pir','ant','tre','ket','pen','šeš'],
			dayNamesMin: ['Sk','Pr','An','Tr','Kt','Pn','Št'],
			weekHeader: 'Wk',
			dateFormat: 'yy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['lt']);
		$.datepicker.setDefaults($.datepicker.regional['lt-LT']);
	}


    if (typeof dateField !== 'undefined' && $.isFunction(dateField)) {
        $('.ipsModuleFormAdmin .type-datefield, .init-datefield').dateField(); //initializing forms if we are in admin interface
        $('.ipsModuleFormAdmin .type-timefield, .init-timefield').timeField(); //initializing forms if we are in admin interface
    }
});

// $('.ipsGrid').on('updateModalOpen.ipGrid', function() {
// 	console.log('a');
//     "use strict";
// 	if ($.datepicker){
// 		$.datepicker.regional['lt'] = {
// 			closeText: 'Uždaryti',
// 			prevText: '&#x3c;Atgal',
// 			nextText: 'Pirmyn&#x3e;',
// 			currentText: 'Šiandien',
// 			monthNames: ['Sausio','Vasario','Kovo','Balandžio','Gegužės','Birželio',
// 			'Liepos','Rugpjūčio','Rugsėjo','Spalio','Lapkričio','Gruodžio'],
// 			monthNamesShort: ['Sau','Vas','Kov','Bal','Geg','Bir',
// 			'Lie','Rugp','Rugs','Spa','Lap','Gru'],
// 			dayNames: ['sekmadienis','pirmadienis','antradienis','trečiadienis','ketvirtadienis','penktadienis','šeštadienis'],
// 			dayNamesShort: ['sek','pir','ant','tre','ket','pen','šeš'],
// 			dayNamesMin: ['Sk','Pr','An','Tr','Kt','Pn','Št'],
// 			weekHeader: 'Wk',
// 			dateFormat: 'yy-mm-dd',
// 			firstDay: 1,
// 			isRTL: false,
// 			showMonthAfterYear: false,
// 			yearSuffix: ''
// 		};
// 		$.datepicker.setDefaults($.datepicker.regional['lt']);
// 		$.datepicker.setDefaults($.datepicker.regional['lt-LT']);
// 	}
//
//     $('.ipsModuleFormAdmin .type-datefield, .init-datefield').dateField(); //initializing forms if we are in admin interface
// });
