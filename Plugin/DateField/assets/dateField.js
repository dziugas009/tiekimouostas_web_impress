/**
 *
 * jQuery object to handle on/off interactions.
 * Selected value is stored in hidden input field.
 *
 * @package ImpressPages
 *
 */


(function($) {
    "use strict";

    var methods = {

        init : function(options) {

            if (typeof(options) === 'undefined') {
                options = {};
            }

            return this.each(function() {

                var $this = $(this);
                var $input = $this.find('input');

                var data = $this.data('dateField');
                if (!data) {
                    $this.data('dateField', {initialized: 1});
                    $input.datepicker({
                        dateFormat: 'yy-mm-dd'
                    });
                    $input.inputmask('y-m-d', {placeholder: 'yyyy-mm-dd', clearIncomplete: true});
                }
            });
        }


    };
	
    $.fn.dateField = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.ipFormRepositoryFile');
        }

    };

})(jQuery);





