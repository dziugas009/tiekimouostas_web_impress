<?php
/**
 * @package   ImpressPages
 */


/**
 * Created by PhpStorm.
 * User: maskas
 * Date: 6/3/14
 * Time: 3:36 PM
 */

namespace Plugin\DateField;


class Event
{
    public static function ipBeforeController()
    {
        //just add js and css
        ipAddJs('assets/jquery.inputmask.bundle.min.js', [], 50);
        ipAddJs('assets/datetimepicker/jquery.datetimepicker.full.min.js');
        ipAddJs('assets/dateField.js?v='.time());
        ipAddJs('assets/timeField.js');
        ipAddJs('assets/initDateTimeField.js');
        ipAddCss('assets/datetimefield.css');
        ipAddCss('assets/datetimepicker/jquery.datetimepicker.css');
    }
}
