<?php
/**
 * @package   ImpressPages
 */


/**
 * Created by PhpStorm.
 * User: maskas
 * Date: 6/3/14
 * Time: 2:48 PM
 */

namespace Plugin\DateField;

/**
 * Main field class responsible to render the field.
 * Class OnOffField
 * @package Plugin\CustomFormField
 */
class TimeField extends \Ip\Form\Field
{

    public function render($doctype, $environment) {
        return '<input type="text" '.$this->getAttributesStr($doctype).' class="form-control '.implode(' ',$this->getClasses()).'" name="'.htmlspecialchars($this->getName()).'" '.$this->getValidationAttributesStr($doctype).' value="'. ( $this->getValue() ? $this->getValue() : '' ) . '" />';
    }

    /**
     * CSS class that should be applied to surrounding element of this field. By default empty. Extending classes should specify their value.
     */
    public function getTypeClass() {
        return 'timefield';
    }


}
