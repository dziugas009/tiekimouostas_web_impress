<?php


$name   = $email = $tel = $company = $text = '';
$errors = [];

if ( isset( $_POST['name'] ) && ! empty( $_POST['name'] ) ) {
	$name = $_POST['name'];
} else {
	$errors[] = array(
		'input'   => 'name',
		'message' => 'Būtina nurodyti vardą'
	);
}
if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) {
	$email = $_POST['email'];
} else {
	$errors[] = array(
		'input'   => 'email',
		'message' => 'Būtina nurodyti el. paštą'
	);
}

if ( isset( $_POST['tel'] ) && ! empty( $_POST['tel'] ) ) {
	$tel = $_POST['tel'];
} else {
	$errors[] = array(
		'input'   => 'tel',
		'message' => 'Būtina nurodyti telefoną'
	);
}
if ( isset( $_POST['company'] ) && ! empty( $_POST['company'] ) ) {
	$company = $_POST['company'];
} else {
	$company = 'nenurodyta';
}
if ( isset( $_POST['text'] ) && ! empty( $_POST['text'] ) ) {
	$text = $_POST['text'];
} else {
	$errors[] = array(
		'input'   => 'text',
		'message' => 'Būtina nurodyti žinutę'
	);
}

if ( ! empty( $errors ) ) {
	echo json_encode( $errors );

} else {
	echo '[]';
	$message = '<div>';
	$message .= '<strong><span style="font-size:20px;">Nauja užklausa</span></strong>';
	$message .= '<br/><strong>Vardas</strong> ' . $name . '</strong>';
	$message .= '<br/><strong>El.Paštas</strong> ' . $email . '</strong>';
	$message .= '<br/><strong>Telefono numeris</strong> ' . $tel . '</strong>';
	$message .= '<br/><strong>Įmonė</strong> ' . $company . '</strong>';
	$message .= '<br/><strong>Žinutė:</strong> ' . $text . '</strong>';
	$headers = "From: Tiekimo uostas <tiekimouostas@tiekimouostas.lt>" . "\r\n";
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	mail( $_POST['myEmail'], 'Naujas pranešimas', $message, $headers );
}

